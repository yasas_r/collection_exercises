package com.nCinga;

import java.util.ArrayList;

public class RemoveDuplicates {

    public static void main(String[] args) {

        ArrayList<Integer> arr = new ArrayList<>();

        arr.add(0);
        arr.add(1);
        arr.add(2);
        arr.add(1);
        arr.add(2);
        arr.add(3);
        arr.add(4);
        arr.add(4);
        arr.add(1);
        arr.add(2);
        printStudentsRecord(arr);
        removeDuplicate(arr);
        System.out.println("removed duplicates");
        printStudentsRecord(arr);

    }

    public static void removeDuplicate(ArrayList<Integer> array) {

        for (int i = 0; i < array.size() - 1; i++) {
            int element1 = array.get(i);

            for (int j = i + 1; j < array.size(); j++) {
                int element2 = array.get(j);
                if (element1 == element2)
                    array.remove(j);
            }
        }

    }

    public static void printStudentsRecord(ArrayList<Integer> array) {
        for (int i = 0; i < array.size(); i++) {
            System.out.println(array.get(i));
        }
    }
}
