package com.nCinga;

import java.util.Stack;

public class ParenthesisCheck {

    public static void main(String[] args) {

        String str1 = "[()]{}{[()()]()}";
        System.out.println(parenthesisCheck(str1));

        String str2 = "[(])";
        System.out.println(parenthesisCheck(str2));
    }

    public static String parenthesisCheck(String input) {
        Stack<Character> stack = new Stack<>();
        for (int i = 0; i < input.length(); i++) {
            Character c = input.charAt(i);
            if (c == '(' || c == '[' || c == '{')
                stack.push(c);
            else if (c == ')') {
                if (!stack.isEmpty() && stack.peek() == '(')
                    stack.pop();
                else
                    return "Not Balanced 1";
            } else if (c == ']') {
                if (!stack.isEmpty() && stack.peek() == '[')
                    stack.pop();
                else
                    return "Not Balanced 2";
            } else if (c == '}') {
                if (!stack.isEmpty() && stack.peek() == '{')
                    stack.pop();
                else
                    return "Not Balanced 3";
            }
        }
        if (stack.isEmpty())
            return "Balanced";
        else {
            System.out.println(stack);
            return "Not Balanced 4";
        }
    }

}
