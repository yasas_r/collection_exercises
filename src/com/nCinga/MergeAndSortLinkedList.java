package com.nCinga;

import java.util.Arrays;
import java.util.LinkedList;
import java.util.ListIterator;

public class MergeAndSortLinkedList {

    public static void main(String[] args) {

        LinkedList<Integer> linkedList1 = new LinkedList<>();
        linkedList1.add(3);
        linkedList1.add(1);
        linkedList1.add(6);
        linkedList1.add(4);
        linkedList1.addFirst(25);
        linkedList1.addLast(30);

        LinkedList<Integer> linkedList2 = new LinkedList<>();
        linkedList2.add(7);
        linkedList2.add(5);
        linkedList2.add(9);
        linkedList2.add(10);

        LinkedList<Integer> listnew = mergeLinkedList(linkedList1, linkedList2);

        System.out.println(listnew);

    }

    public static LinkedList mergeLinkedList(LinkedList<Integer> list1, LinkedList<Integer> list2) {

        LinkedList<Integer> resultList = new LinkedList<Integer>();


        for (int i = 0; i < list1.size(); i++) {
            resultList.add(list1.get(i));
            resultList.sort(Integer::compareTo);
        }
        ListIterator itr2 = resultList.listIterator();
        for (int j = 0; j < list2.size(); j++) {
            resultList.add(list2.get(j));
            resultList.sort(Integer::compareTo);
        }

        return resultList;
    }
}
