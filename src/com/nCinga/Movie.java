package com.nCinga;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Iterator;

public class Movie implements Comparable<Movie> {

    public static void main(String[] args) {

        ArrayList<Movie> list = new ArrayList<Movie>();
        list.add(new Movie("Bat Man", 8.3, 2015));
        list.add(new Movie("Star Wars", 7.5, 2017));
        list.add(new Movie("Iron Man", 8.8, 2020));
        list.add(new Movie("Spider Man", 7.7, 2014));

        Collections.sort(list);
        Iterator itr1 = list.iterator();

        while (itr1.hasNext()) {
            Movie movie = (Movie) itr1.next();
            System.out.println(movie.toString());
        }
        System.out.println("--------------");

        Collections.sort(list, new sortByRating());
        Iterator itr2 = list.iterator();

        while (itr2.hasNext()) {
            Movie movie = (Movie) itr2.next();
            System.out.println(movie.toString());
        }
        System.out.println("--------------");

    }

    private String name;
    private Double rating;
    private int releaseYear;

    public Movie(String name, Double rating, int releaseYear) {
        this.name = name;
        this.rating = rating;
        this.releaseYear = releaseYear;
    }

    public String getName() {
        return name;
    }

    public Double getRating() {
        return rating;
    }

    public int getReleaseYear() {
        return releaseYear;
    }

    @Override
    public int compareTo(Movie movie) {
        return this.getName().compareTo(movie.getName()); //ascending
        //return movie.getName().compareTo(this.getName()); //descending
    }

    @Override
    public String toString() {
        return "Movie{" +
                "name='" + name + '\'' +
                ", rating=" + rating +
                ", releaseYear=" + releaseYear +
                '}';
    }

}

class sortByRating implements Comparator<Movie> {

    @Override
    public int compare(Movie movie1, Movie movie2) {

        if (movie1.getRating() < movie2.getRating())
            return -1;
        else if (movie1.getRating() > movie2.getRating())
            return 1;
        else
            return 0;
    }
}
