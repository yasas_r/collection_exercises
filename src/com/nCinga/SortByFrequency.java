package com.nCinga;

import java.util.*;

public class SortByFrequency {

    public static void main(String[] args) {

        ArrayList<Integer> arrayList = new ArrayList<>();

        arrayList.add(1);
        arrayList.add(5);
        arrayList.add(3);
        arrayList.add(6);
        arrayList.add(5);
        arrayList.add(2);
        arrayList.add(3);
        arrayList.add(7);
        arrayList.add(3);
        arrayList.add(8);
        arrayList.add(8);

        arrayList=sortByFrequency(arrayList);

        System.out.println(arrayList);

    }

    public static ArrayList sortByFrequency(ArrayList<Integer> arrayList){

        HashMap<Integer,Integer> hashMap = new HashMap<>();
        ArrayList<Integer> result = new ArrayList<>();

        for(int element :arrayList){
            int count = hashMap.getOrDefault(element,0);
            hashMap.put(element,count+1);
            result.add(element);
        }

        Collections.sort(result,new Frequency(hashMap));

        return result;

    }

    static class Frequency implements Comparator<Integer> {

        private HashMap<Integer, Integer> hashMap;

        Frequency(HashMap<Integer, Integer> hashMap)
        {
            this.hashMap = hashMap;
        }

        @Override
        public int compare(Integer o1, Integer o2) {

            int freValue1 = hashMap.get(o1);
            int freValue2 = hashMap.get(o2);

            if(freValue1==freValue2)
                return 0;
            else
                return freValue2-freValue1;
        }
    }
}