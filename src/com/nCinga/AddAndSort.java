package com.nCinga;

import java.util.Comparator;
import java.util.Iterator;
import java.util.PriorityQueue;
import java.util.TreeSet;

public class AddAndSort {

    public static void main(String[] args) {

        //---TreeSet with default order{String class comparable}
        TreeSet<String> ts = new TreeSet<String>();
        ts.add(new String("A"));
        ts.add(new String("Z"));
        ts.add(new String("L"));
        ts.add(new String("B"));
        ts.add(new String("O"));
        System.out.println(ts);

        //---TreeSet with default order{ Movie class comparable Override method}
        TreeSet<Movie> ts2 = new TreeSet<Movie>();
        ts2.add(new Movie("Bat Man", 8.3, 2015));
        ts2.add(new Movie("Star Wars", 7.5, 2017));
        ts2.add(new Movie("Iron Man", 8.8, 2020));
        ts2.add(new Movie("Spider Man", 7.7, 2014));
        System.out.println(ts2);

        //---- TreeSet with Movie class and Comparator--sortByRating
        TreeSet<Movie> ts3 = new TreeSet<Movie>(ratingComparator);
        ts3.add(new Movie("Bat Man", 8.3, 2015));
        ts3.add(new Movie("Star Wars", 7.5, 2017));
        ts3.add(new Movie("Iron Man", 8.8, 2020));
        ts3.add(new Movie("Spider Man", 7.7, 2014));
        System.out.println(ts3);
        System.out.println(ts3.remove(new Movie("Star Wars", 7.5, 2017)));
        System.out.println(ts3);

        PriorityQueue<Movie> priorityQueue = new PriorityQueue<Movie>(ratingComparator);

        priorityQueue.add(new Movie("Bat Man", 8.3, 2015));
        priorityQueue.add(new Movie("Star Wars", 7.5, 2017));
        priorityQueue.add(new Movie("Iron Man", 8.8, 2020));
        priorityQueue.add(new Movie("Spider Man", 7.7, 2014));

        Iterator itr1 = priorityQueue.iterator();
        System.out.println("--------------");
        while (itr1.hasNext()) {
            Movie movie = (Movie) itr1.next();
            System.out.println(movie.toString());
        }
        System.out.println("--------------");

    }

    public static Comparator<Movie> ratingComparator = new Comparator<Movie>() {

        @Override
        public int compare(Movie movie1, Movie movie2) {
            if (movie1.getRating() < movie2.getRating())
                return -1;
            else if (movie1.getRating() > movie2.getRating())
                return 1;
            else
                return 0;
        }
    };
}
