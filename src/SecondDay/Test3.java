package SecondDay;

interface m1 {
    default void eat() {
        System.out.println("Eating in m1");
    }
}
interface m2 {
    default void eat() {
        System.out.println("Eating in m2");
    }
}

public class Test3 implements m1, m2{

    @Override
    public void eat() {
        m1.super.eat();
        m2.super.eat();
    }

    public static void main(String[] args) {
        Test3 obj = new Test3();

        obj.eat();
    }

}
