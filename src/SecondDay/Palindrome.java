package SecondDay;

import java.util.Arrays;
import java.util.List;
import java.util.function.Consumer;

public class Palindrome {

    public static void main(String[] args) {
        List<String> names = Arrays.asList("Sam","Bob","Tom","aba","Madam");
        checkPalindrome1(names);
        System.out.println("-------------");
        checkPalindrome2(names);

    }

    public static void checkPalindrome1(List<String> strings){

        Consumer<String> checkPalin = str -> {
            if(str.equalsIgnoreCase( new StringBuilder(str).reverse().toString() ))
                System.out.println("This is a palindrome");
            else
                System.out.println("This is not a palindrome");

        };
        strings.forEach(checkPalin);


    }

    public static void checkPalindrome2(List<String> strings){

        Consumer<String> checkPalin = str -> {
            if(isPalindrome(str))
                System.out.println("This is a palindrome");
            else
                System.out.println("This is not a palindrome");

        };
        strings.forEach(checkPalin);


    }

    public static boolean isPalindrome(String string){

        string = string.toLowerCase();

        int length = string.length();

        if(length == 0 || length == 1)
            return true;

        for(int i = 0; i < (length/2) ; i++){

            if( string.charAt(i) != string.charAt(length - 1 - i) )
                return false;
        }

        return true;
    }


}
