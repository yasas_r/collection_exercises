package SecondDay;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class StreamTest {

    public static void main(String[] args) {
        ////////////////////////////// Test Stream
        List<Integer> nums = Arrays.asList(6,7,8,9);
        Stream<Integer> s2 = nums.stream();

        List<String> result = Stream.of("Hello")
                .filter(a->a.startsWith("H"))  //filter
                .map(a->a.toUpperCase())  //map
                .collect(Collectors.toList());

        s2.sorted().forEach(System.out::println);  //sort

        String isFound =Stream.of("Hello").filter(a->a.startsWith("H"))
                .findFirst()
                .orElse("Non found");
        System.out.println(isFound);
        /////////////////////////// Test Stream

        List<Integer> numList = Arrays.asList(6,7,8,9);

        Stream<Integer> stream1 = numList.stream();
        System.out.println(sumStreamElement(stream1));

        Stream<Integer> stream2 = numList.stream();
        reverseStreamElement(stream2);

    }

    public static int sumStreamElement(Stream<Integer> stream){

        int sum = stream.reduce(0,(acc, item) -> item + acc);

        return sum;

    }

    public static void reverseStreamElement(Stream<Integer> stream){

        stream.sorted(Collections.reverseOrder())
                .forEach(System.out::println);

    }



}
