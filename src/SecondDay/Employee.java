package SecondDay;

import java.util.Optional;
import java.util.function.Predicate;

public class Employee {

    String name;
    int salary;

    public Employee() {

    }

    public Employee(String name, int salary) {
        this.name = name;
        this.salary = salary;
    }

    public static void main(String[] args) {

        Optional<Employee> empOpt1 = Optional.of(new Employee("Sam", 12000));

        Predicate<Employee> p = x -> (x.name.startsWith("S"));

        System.out.println(empOpt1.filter(p));
        System.out.println(empOpt1.filter(p).orElse(new Employee("Sim", 14000)));


    }
}